<?php


function event_shortcode_display_post($attr, $content = null){
 
 //call global variable
     global $post;

 // Defines Shortcode's Attributes
 $shortcode_args = shortcode_atts(
                   array(
                     'cat'     => '',
                     'num'     => '',
                     'order'  => ''
                     ), $attr);    
  
 // set an array with query arguments
 $args = array(
     'post_type' => 'events',
                 'taxanomy'         => $shortcode_args['cat'],
         'posts_per_page' => $shortcode_args['num'],
         'order'          => $shortcode_args['order']
          );
  
 // get posts  
 $recent_posts = get_posts($args);


  
 // extracting each post by foreach loop
 foreach ($recent_posts as $post) :
      
     //  set new post data
     echo the_post_thumbnail();

     echo "<h1>".the_title()."</h1>";

 endforeach;    
  
 // reset post data  
 wp_reset_postdata();





}

// Register the shortcode.
add_shortcode( 'event_recent_posts', 'event_shortcode_display_post' );