<?php
/*
 * Function Name: my_custom_post_type
 * Creating Custom Post type for Portfolio Post 
*/
function my_custom_post_type_event() {
	register_post_type('events',
		array(
			'labels'        => array(
				'name'          => __('Events', 'corlate'),
				'singular_name' => __('Event', 'corlate'),
			),
			'public'        => true,
			'has_archive'   => true,
			'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes'),
			'rewrite' => array('slug' => 'event-post'),
				
			'menu_icon'     =>  'dashicons-format-aside'
		)
	);
}
add_action('init', 'my_custom_post_type_event');

/*
 * Function Name: my_taxanomy
 * Creating Taxanomies for Custom Post type Portfolio Post 
*/
function my_taxonomy_event(){
	$labels = array(
		'name'              => _x( 'Events Type', 'taxonomy general name' ),
		'singular_name'     => _x( 'Event Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Event Types' ),
		'all_items'         => __( 'All Types' ),
		'parent_item'       => __( 'Parent Type' ),
		'parent_item_colon' => __( 'Parent Types' ),
		'edit_item'         => __( 'Edit Event Type' ),
		'update_item'       => __( 'Update Event Type' ),
		'add_new_item'      => __( 'Add New Event Type' ),
		'new_item_name'     => __( 'New Event Type Name' ),
		'menu_name'         => __( 'Event Type' ),
	);
	$args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'rewrite'           => [ 'slug' => 'event-type' ],
     );
	register_taxonomy( 'event-type', array('events'), $args );
}
add_action( 'init', 'my_taxonomy_event' );