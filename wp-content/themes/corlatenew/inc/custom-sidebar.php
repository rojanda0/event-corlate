<?php 
/*
 * Function Name: my_register_sidebars
 * Creating Custom Sidebars 
*/
add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'my-sidebar',
            'name'          => __( 'sidebar1' ),
            'description'   => __( '1st sidebar for posts' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '',
            'after_title'   => '',
        )
    );
}