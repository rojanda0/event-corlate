********************************************************************************
* DUPLICATOR-PRO: Install-Log
* STEP-1 START @ 11:37:03
* VERSION: 1.4.3
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.2.34                                |7.3.29
OS__________________: Linux                                 |WINNT
CREATED_____________: 2021-09-09 11:33:19
WP VERSION__________: 4.7.21
DUP VERSION_________: 1.4.3
DB__________________: 5.7.35
DB TABLES___________: 22
DB ROWS_____________: 6,445
DB FILE SIZE________: 28.19MB
********************************************************************************
SERVER INFO
PHP_________________: 7.3.29 | SAPI: apache2handler
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: Apache/2.4.48 (Win64) OpenSSL/1.1.1k PHP/7.3.29
DOC ROOT____________: "C:/xampp/htdocs/acpnepal"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "http://localhost/acpnepal/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ACTION______: "donothing"
ARCHIVE ENGINE______: "ziparchive"
SET DIR PERMS_______: 0
DIR PERMS VALUE_____: 644
SET FILE PERMS______: 0
FILE PERMS VALUE____: 755
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "C:/xampp/htdocs/acpnepal/20210909_acpnepal_[HASH]_20210909113319_archive.zip"
SIZE________________: 42.61MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.
BEFORE EXTRACION ACTIONS
MAINTENANCE MODE ENABLE


START ZIP FILE EXTRACTION STANDARD >>> 

--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "C:/xampp/htdocs/acpnepal/20210909_acpnepal_[HASH]_20210909113319_archive.zip"
SIZE________________: 42.61MBFile timestamp set to Current: 2021-09-09 11:38:13
<<< ZipArchive Unzip Complete: true
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------

PERMISSION UPDATES: None Applied
- PASS: Existing WordFence '.user.ini' was removed

STEP-1 COMPLETE @ 11:38:13 - RUNTIME: 69.6357 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 11:39:11
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
FUNCTION CREATION___: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.4.20 -- Build Server: 5.7.35
FILE SIZE:	dup-database__[HASH].sql (17.82MB)
TIMEOUT:	5000
MAXPACK:	1048576
SQLMODE:	NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[C:/xampp/htdocs/acpnepal/dup-installer/dup-installer-data__[HASH].sql]
COLLATE FB:	Off
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
DB FUNCTIONS:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	0
RENAMED TABLES:	0
QUERIES RAN:	7341

wp_commentmeta: (30)
wp_comments: (28)
wp_duplicator_packages: (0)
wp_links: (0)
wp_options: (210)
wp_postmeta: (4819)
wp_posts: (1850)
wp_revslider_css: (113)
wp_revslider_layer_animations: (3)
wp_revslider_navigations: (0)
wp_revslider_sliders: (3)
wp_revslider_slides: (13)
wp_revslider_static_slides: (1)
wp_term_relationships: (92)
wp_term_taxonomy: (22)
wp_termmeta: (0)
wp_terms: (22)
wp_usermeta: (29)
wp_users: (1)
wp_wfu_dbxqueue: (0)
wp_wfu_log: (3)
wp_wfu_userdata: (0)
Removed '77' cache/transient rows

INSERT DATA RUNTIME: 30.5817 sec.
STEP-2 COMPLETE @ 11:39:42 - RUNTIME: 30.5948 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 11:39:49
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "acpnepal"
postguid______________: false
fullsearch____________: false
path_old______________: "/home/acpnepalcom/public_html"
path_new______________: "C:/xampp/htdocs/acpnepal"
siteurl_______________: "http://localhost/acpnepal"
url_old_______________: "http://acpnepal.com.np"
url_new_______________: "http://localhost/acpnepal"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: false
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "wp_commentmeta"__________________________________[ROWS:    30][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_comments"_____________________________________[ROWS:    28][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_duplicator_packages"__________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_links"________________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_options"______________________________________[ROWS:   210][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_postmeta"_____________________________________[ROWS:  4819][PG:   2][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_posts"________________________________________[ROWS:  1850][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_revslider_css"________________________________[ROWS:   113][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_revslider_layer_animations"___________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_revslider_navigations"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_sliders"____________________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_revslider_slides"_____________________________[ROWS:    13][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_revslider_static_slides"______________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_term_relationships"___________________________[ROWS:    92][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_term_taxonomy"________________________________[ROWS:    22][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_termmeta"_____________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_terms"________________________________________[ROWS:    22][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_usermeta"_____________________________________[ROWS:    29][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_users"________________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_wfu_dbxqueue"_________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wfu_log"______________________________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/acpnepalcom/public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  2:"\/home\/acpnepalcom\/public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  3:"%2Fhome%2Facpnepalcom%2Fpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  4:"\home\acpnepalcom\public_html" ===================> "C:/xampp/htdocs/acpnepal"
	SEARCH  5:"\\home\\acpnepalcom\\public_html" ================> "C:\/xampp\/htdocs\/acpnepal"
	SEARCH  6:"%5Chome%5Cacpnepalcom%5Cpublic_html" =============> "C%3A%2Fxampp%2Fhtdocs%2Facpnepal"
	SEARCH  7:"//acpnepal.com.np" ===============================> "//localhost/acpnepal"
	SEARCH  8:"\/\/acpnepal.com.np" =============================> "\/\/localhost\/acpnepal"
	SEARCH  9:"%2F%2Facpnepal.com.np" ===========================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 10:"//www.acpnepal.com.np" ===========================> "//localhost/acpnepal"
	SEARCH 11:"\/\/www.acpnepal.com.np" =========================> "\/\/localhost\/acpnepal"
	SEARCH 12:"%2F%2Fwww.acpnepal.com.np" =======================> "%2F%2Flocalhost%2Facpnepal"
	SEARCH 13:"https://localhost/acpnepal" ======================> "http://localhost/acpnepal"
	SEARCH 14:"https:\/\/localhost\/acpnepal" ===================> "http:\/\/localhost\/acpnepal"
	SEARCH 15:"https%3A%2F%2Flocalhost%2Facpnepal" ==============> "http%3A%2F%2Flocalhost%2Facpnepal"

EVALUATE TABLE: "wp_wfu_userdata"_________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]
--------------------------------------
SCANNED:	Tables:22 	|	 Rows:7239 	|	 Cells:64670 
UPDATED:	Tables:4 	|	 Rows:922 	|	 Cells:1312 
ERRORS:		0 
RUNTIME:	11.053600 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME "** OBSCURED **"
	UPDATE DB_USER "** OBSCURED **"
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache '.htaccess__[HASH]' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Deactivated plugins list here: Array
(
    [0] => really-simple-ssl/rlrsssl-really-simple-ssl.php
    [1] => simple-google-recaptcha/simple-google-recaptcha.php
    [2] => js_composer/js_composer.php
)

deactivate js_composer/js_composer.php
Plugin(s) listed here are deactivated: js_composer/js_composer.php
Plugin(s) reactivated after installation: js_composer/js_composer.php
MAINTENANCE MODE DISABLE

====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 11:40:01 - RUNTIME: 12.1756 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] Info
	SECTIONS: general
	LONG MSG:             The following is a list of notices that may need to be fixed in order to finalize your setup. These values should only be investigated if you're running into
            issues with your site. For more details see the <a href="https://codex.wordpress.org/Editing_wp-config.php" target="_blank">WordPress Codex</a>.

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

-----------------------
[WARNING] Deactivated Plugin:  WPBakery Page Builder
	SECTIONS: general
	LONG MSG: This plugin is deactivated automatically, because it requires a reacivation to work properly.  <b>Please reactivate from the WordPress admin panel after logging in.</b> This will re-enable your site's frontend.

====================================
